package com.example.su_rx_java;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observables.ConnectableObservable;

public class MainActivity extends AppCompatActivity {

    //    Операторы создания fromArray, range, interval
    Observable<String> observableDemo = Observable.fromArray("one", "two", "three");
    Observable<Integer> observableRange = Observable.range(10, 4);
    Observable<Long> observableInterval = Observable.interval(500, TimeUnit.MILLISECONDS);

    //    Операторы преобразования map, buffer,
    Function<String, Integer> stringToInteger = new Function<String, Integer>() {
        @Override
        public Integer apply(String s) throws Exception {
            return Integer.parseInt(s);
        }
    };

    Observable<Integer> observableMap = Observable
            .fromArray("1", "2", "3", "5", "6")
            .map(stringToInteger);

    Observable<List<Integer>> observableBuffer = Observable
            .fromArray(1, 2, 3, 4, 5, 6, 7, 8)
            .buffer(3);

    //    Операторы фильтрации take, skip, filter, distinct
    Observable<Integer> observableTake = Observable
            .fromArray(5, 6, 7, 8, 9)
            .take(3);

    Observable<Integer> observableSkip = Observable
            .fromArray(5, 6, 7, 8, 9)
            .skip(2);

    Observable<Integer> observableDistinct = Observable
            .fromArray(5, 9, 7, 5, 8, 6, 7, 8, 9)
            .distinct();

    Predicate<String> filterFiveOnly = new Predicate<String>() {
        @Override
        public boolean test(String s) throws Exception {
            return s.contains("5");
        }
    };
    Observable<String> observableFilter = Observable
            .fromArray("15", "27", "34", "46", "52", "63")
            .filter(filterFiveOnly);

    //    Операторы объединения merge, zip,
    Observable<Integer> observableMerge = Observable
            .fromArray(1, 2, 3)
            .mergeWith(Observable.fromArray(6, 7, 8, 9));

    BiFunction<Integer, String, String> zipIntWithString = new BiFunction<Integer, String, String>() {
        @Override
        public String apply(Integer integer, String s) throws Exception {
            return s + ": " + integer;
        }
    };

    Observable<String> observableZip = Observable
            .fromArray(1, 2, 3)
            .zipWith(Observable.fromArray("One", "Two", "Three"), zipIntWithString);

    //    Операторы условий takeUntil, all,
    Predicate<Integer> isFive = new Predicate<Integer>() {
        @Override
        public boolean test(Integer integer) throws Exception {
            return integer == 5;
        }
    };

    Observable<Integer> observableTakeUntil = Observable
            .fromArray(1, 2, 3, 4, 5, 6, 7, 8)
            .takeUntil(isFive);

    Predicate<Integer> lessThanTen = new Predicate<Integer>() {
        @Override
        public boolean test(Integer integer) throws Exception {
            return integer < 10;
        }
    };

    Single<Boolean> observableAll = Observable
            .fromArray(1,2,3,4,5,6,7,8)
            .all(lessThanTen);

    Observer<String> observerDemo = new Observer<String>() {
        @Override
        public void onSubscribe(Disposable d) {
            log("onSubcribe");
        }

        @Override
        public void onNext(String s) {
            log("onNext: " + s);
        }

        @Override
        public void onError(Throwable e) {
            log("onError: " + e);
        }

        @Override
        public void onComplete() {
            log("onComplete");
        }
    };

    Observer<Integer> observerRange = new Observer<Integer>() {
        @Override
        public void onSubscribe(Disposable d) {
            log("observerRange onSubcribe");
        }

        @Override
        public void onNext(Integer s) {
            log("observerRange onNext: " + s);
        }

        @Override
        public void onError(Throwable e) {
            log("observerRange onError: " + e);
        }

        @Override
        public void onComplete() {
            log("observerRange onComplete");
        }
    };

    Observer<Long> observerInterval = new Observer<Long>() {
        @Override
        public void onSubscribe(Disposable d) {
            log("observerInterval onSubcribe");
        }

        @Override
        public void onNext(Long s) {
            log("observerInterval onNext: " + s);
        }

        @Override
        public void onError(Throwable e) {
            log("observerInterval onError: " + e);
        }

        @Override
        public void onComplete() {
            log("observerInterval onComplete");
        }
    };

    Observer<List<Integer>> observerBuffer = new Observer<List<Integer>>() {
        @Override
        public void onSubscribe(Disposable d) {
            log("observerBuffer onSubcribe");
        }

        @Override
        public void onNext(List<Integer> s) {
            log("observerBuffer onNext: " + s);
        }

        @Override
        public void onError(Throwable e) {
            log("observerBuffer onError: " + e);
        }

        @Override
        public void onComplete() {
            log("observerBuffer onComplete");
        }
    };

    Observer<Boolean> observerBoolean = new Observer<Boolean>() {
        @Override
        public void onSubscribe(Disposable d) {
            log("onSubcribe");
        }

        @Override
        public void onNext(Boolean s) {
            log("onNext: " + s);
        }

        @Override
        public void onError(Throwable e) {
            log("onError: " + e);
        }

        @Override
        public void onComplete() {
            log("onComplete");
        }
    };

    SingleObserver<Boolean> singleObserver = new SingleObserver<Boolean>() {
        @Override
        public void onSubscribe(Disposable d) {
            log("singleObserver onSubscribe");
        }

        @Override
        public void onSuccess(Boolean aBoolean) {
            log("singleObserver onSuccess : " + aBoolean);
        }

        @Override
        public void onError(Throwable e) {
            log("singleObserver onError: " + e.getLocalizedMessage());
        }
    };

//    Consumer in RX 1 was Action
    Consumer<String> simpleAction = new Consumer<String>() {
        @Override
        public void accept(String s) throws Exception {
            log("simpleAction accept: " + s);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        observableDemo.subscribe(observerDemo);
//        observableRange.subscribe(observerRange);
//        observableInterval.subscribe(observerInterval);
//        observableMap.subscribe(observerRange);
//        observableBuffer.subscribe(observerBuffer);
//        observableTake.subscribe(observerRange);
//        observableSkip.subscribe(observerRange);
//        observableDistinct.subscribe(observerRange);
//        observableFilter.subscribe(observerDemo);
//        observableMerge.subscribe(observerRange);
//        observableZip.subscribe(observerDemo);
//        observableTakeUntil.subscribe(observerRange);
//        observableAll.subscribe(singleObserver);
//        observableDemo.subscribe(simpleAction);

//        subscriptionDemo();
//        compositeDemo();

//        coldObserverDemo();
        hotObserverDemo();
    }



    private void subscriptionDemo() {
//        Disposable in RX 1 was Subscription
        final Disposable subscription = observableDemo.subscribe(simpleAction);
        getWindow().getDecorView().postDelayed(new Runnable() {
            @Override
            public void run() {
                log("unsubscribe");
                subscription.dispose();
            }
        }, 4500);
    }

    private void compositeDemo() {

        Consumer<Long> simpleAction1 = new Consumer<Long>() {
            @Override
            public void accept(Long s) throws Exception {
                log("simpleAction1 accept: " + s);
            }
        };

        Consumer<Long> simpleAction2 = new Consumer<Long>() {
            @Override
            public void accept(Long s) throws Exception {
                log("simpleAction2 accept: " + s);
            }
        };

        final Disposable subscription1 = observableInterval.subscribe(simpleAction1);
        final Disposable subscription2 = observableInterval.subscribe(simpleAction2);

        CompositeDisposable compositeSubscription = new CompositeDisposable();
        compositeSubscription.add(subscription1);
        compositeSubscription.add(subscription2);
        log("subscription1 is unsubscribed " + subscription1.isDisposed());
        log("subscription2 is unsubscribed " + subscription2.isDisposed());

        compositeSubscription.dispose();

        log("subscription1 is unsubscribed " + subscription1.isDisposed());
        log("subscription2 is unsubscribed " + subscription2.isDisposed());
    }


    private void coldObserverDemo(){
        final Observer<Long> observer1 = new Observer<Long>() {
            @Override
            public void onError(Throwable e) {
                log("observer1 onError");
            }

            @Override
            public void onComplete() {
                log("observer1 onComplete");
            }

            @Override
            public void onSubscribe(Disposable d) {
                log("observer1 onSubscribe");
            }

            @Override
            public void onNext(Long aLong) {
                log("observer1 onNext value = " + aLong);
            }
        };

        final Observer<Long> observer2 = new Observer<Long>() {

            @Override
            public void onError(Throwable e) {
                log("observer2 onError");
            }

            @Override
            public void onComplete() {
                log("observer2 onComplete");
            }

            @Override
            public void onSubscribe(Disposable d) {
                log("observer2 onSubscribe");
            }

            @Override
            public void onNext(Long aLong) {
                log("observer2 onNext value = " + aLong);
            }
        };

        log("observable create");
        final Observable<Long> observable = Observable
                .interval(1, TimeUnit.SECONDS)
                .take(5);

        getWindow().getDecorView().postDelayed(new Runnable() {
            @Override
            public void run() {
                log("observer1 subscribe");
                observable.subscribe(observer1);
            }
        }, 3000);

        getWindow().getDecorView().postDelayed(new Runnable() {
            @Override
            public void run() {
                log("observer2 subscribe");
                observable.subscribe(observer2);
            }
        }, 5500);
    }


    private void hotObserverDemo(){
        final Observer<Long> observer1 = new Observer<Long>() {
            @Override
            public void onError(Throwable e) {
                log("observer1 onError");
            }

            @Override
            public void onComplete() {
                log("observer1 onComplete");
            }

            @Override
            public void onSubscribe(Disposable d) {
                log("observer1 onSubscribe");
            }

            @Override
            public void onNext(Long aLong) {
                log("observer1 onNext value = " + aLong);
            }
        };

        final Observer<Long> observer2 = new Observer<Long>() {

            @Override
            public void onError(Throwable e) {
                log("observer2 onError");
            }

            @Override
            public void onComplete() {
                log("observer2 onComplete");
            }

            @Override
            public void onSubscribe(Disposable d) {
                log("observer2 onSubscribe");
            }

            @Override
            public void onNext(Long aLong) {
                log("observer2 onNext value = " + aLong);
            }
        };

        log("observable create");
        final ConnectableObservable<Long> observable = Observable
                .interval(1, TimeUnit.SECONDS)
                .take(6)
                .publish();

        log("observable connect");
        observable.connect();

        getWindow().getDecorView().postDelayed(new Runnable() {
            @Override
            public void run() {
                log("observer1 subscribe");
                observable.subscribe(observer1);
            }
        }, 3000);

        getWindow().getDecorView().postDelayed(new Runnable() {
            @Override
            public void run() {
                log("observer2 subscribe");
                observable.subscribe(observer2);
            }
        }, 5500);
    }


    private void log(String s) {
        Log.w("TODEL", "line : " + Thread.currentThread().getStackTrace()[2].getLineNumber() + " - MainActivity. log: " + s);
    }


}
